

var user_choice_time;
var t = null;
var x = null;


/**
 * Initialize the countdown
 *
 * @param timer
 */
function init(timer) {

    var time;

    if(timer) { // If refresh page, get the time value in sessionStorage
        time = new Date().getTime() + timer;
    } else {
        alert('Veuillez entrez une durée !!')
    }

    x = setInterval(function () { // Update the count down every 1 second

        var now = new Date().getTime(); // Get todays date and time
        var interval = time - now;        // Find the distance between now an the count down date

        localStorage.setItem('interval', interval);

        // Time calculations for days, hours, minutes and seconds
        var minutes = Math.floor((interval % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((interval  % (1000 * 60)) / 1000);

        localStorage.setItem('min', minutes);
        localStorage.setItem('sec', seconds);

        if (minutes !== -1 && seconds !== -1) {

            var blocMin = document.getElementById("min");
            var blocSec = document.getElementById("sec");

            // Output the result in an element with id="demo"
            blocMin.innerHTML = localStorage.getItem('min');
            blocSec.innerHTML = localStorage.getItem('sec');

        }
        else {// If the count down is over, write some text
            stop();
        }
    }, 1000);

}

/**
 * Stop and clear the countdown
 */
function stop() {
    clearInterval(x);
    clear();
    localStorage.clear();
    localStorage.setItem('isActive', false)
}

/**
 * Clear the display
 */
function clear() {
    $('#min').empty();
    $('#sec').empty();
}

/**
 * Ask to user where minutes number
 */
function answer_user() {
    var question = Number(prompt("Initialisez le nombre de minute du compte à rebours :"));
    t = question * 60 * 1000;
}




/**
 * Verify is countdown is active and rrestart it if is true
 */
if(localStorage.getItem('isActive') === true) {
    t = parseInt(localStorage.getItem('interval'));
    init(t);
}





// Button Handler //
$('#start').on('click', function() {
    localStorage.setItem('isActive', true)
    answer_user();
    init(t);
});

$('#stop').on('click', function() {
    stop();
});